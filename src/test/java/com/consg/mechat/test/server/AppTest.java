package com.consg.mechat.test.server;

import org.junit.Test;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;

public class AppTest {

  @Test
  public void test() {

    Instant now = Instant.now();
    ZonedDateTime zonedDateTime = now.atZone(ZoneOffset.UTC);
    System.out.println(zonedDateTime);

    LocalDateTime endDate = LocalDateTime.ofInstant(now, ZoneOffset.UTC);
    System.out.println(endDate);

    System.out.println(ZonedDateTime.now(ZoneOffset.UTC));
    System.out.println(LocalDateTime.now(ZoneOffset.UTC));

  }

}